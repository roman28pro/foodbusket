package foodbasket.controllers;

import foodbasket.entities.Group;
import foodbasket.entities.Person;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalcControllerTest {

    @Test
    public void calc() {
        Person.setGroup(Group.HIGH_BUDGET);
        double result = new CalcController().calc(5,5,5,
                5,5,5,5,5,
                5,5,5,5);
        assertEquals(result, 15175.0, 5);

        Person.setGroup(Group.AVERAGE_BUDGET);
        result = new CalcController().calc(3,3,3,
                3,3,3,3,3,
                3,3,3,3);
        assertEquals(result, 5370.0, 5);

        Person.setGroup(Group.LOW_BUDGET);
        result = new CalcController().calc(1,1,1,
                1,1,1,1,1,
                1,1,1,1);
        assertEquals(result, 1167.0, 5);
    }
}
