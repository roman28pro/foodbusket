package foodbasket.utils;

import org.junit.Test;
import java.sql.SQLException;

public class DataBaseTest {

    @Test(expected = SQLException.class)
    public void insert() throws SQLException {
        new DataBase().insert(5,5,
                5,5,5,5,
                5,5,5,5,5,
                5,null);
    }
}