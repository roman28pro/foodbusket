package foodbasket.utils;

import foodbasket.entities.Group;
import org.junit.Test;

import static org.junit.Assert.*;

public class GroupFactoryTest {

    @Test
    public void getAverageGroup() {
        Group group = GroupFactory.getGroup(17);
        assertEquals(group, Group.AVERAGE_BUDGET);
    }

    @Test
    public void getHighGroup() {
        Group group = GroupFactory.getGroup(18);
        assertEquals(group, Group.HIGH_BUDGET);
    }

    @Test
    public void getLowGroup() {
        Group group = GroupFactory.getGroup(70);
        assertEquals(group, Group.LOW_BUDGET);
    }

    @Test
    public void getAllGroup() {
        for (int i = 0; i < 18; i++) {
            Group group = GroupFactory.getGroup(i);
            assertEquals(group, Group.AVERAGE_BUDGET);
        }

        for (int i = 18; i < 65; i++) {
            Group group = GroupFactory.getGroup(i);
            assertEquals(group, Group.HIGH_BUDGET);
        }

        for (int i = 65; i < 100; i++) {
            Group group = GroupFactory.getGroup(i);
            assertEquals(group, Group.LOW_BUDGET);
        }
    }
}
