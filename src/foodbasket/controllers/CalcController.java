package foodbasket.controllers;

import foodbasket.entities.Person;
import foodbasket.popupwindows.DBWindow;
import foodbasket.popupwindows.ErrorWindow;
import foodbasket.utils.DataBase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.sql.SQLException;

@SuppressWarnings("all")
public class CalcController {

    @FXML
    private Label groupLabel;

    @FXML
    private Label bakeryLabel;

    @FXML
    private Label potatoLabel;

    @FXML
    private Label sugarLabel;

    @FXML
    private Label meatLabel;

    @FXML
    private Label fishLabel;

    @FXML
    private Label milkLabel;

    @FXML
    private Label eggsLabel;

    @FXML
    private Label butterLabel;

    @FXML
    private Label teaLabel;

    @FXML
    private TextField bakeryInput;

    @FXML
    private TextField pastaAndCerealsInput;

    @FXML
    private TextField potatoInput;

    @FXML
    private TextField vegetablesInput;

    @FXML
    private TextField fruitInput;

    @FXML
    private TextField sugarInput;

    @FXML
    private TextField meatInput;

    @FXML
    private TextField fishInput;

    @FXML
    private TextField milkInput;

    @FXML
    private TextField eggsInput;

    @FXML
    private TextField butterInput;

    @FXML
    private TextField teaInput;

    @FXML
    private Button readDBButton;

    @FXML
    private Button resultButton;

    @FXML
    private Button writeDBButton;

    @FXML
    private Label pastaAndCerealsLabel;

    @FXML
    private Label fruitLabel;

    @FXML
    private Label vegetablesLable;

    @FXML
    private Label resultLabel;

    /**
     *
     * @param event
     */

    @FXML
    void showDB(ActionEvent event) {
        DBWindow.openWindow();
    }

    /**
     *
     * @param bakeryPrice - цена хлебобулочных изделий
     * @param pastaAndCerealsPrice - цена макаронных изделий и круп
     * @param potatoPrice - цена картофеля
     * @param vegetablesPrice - цена овощей
     * @param fruitPrice - цена фруктов
     * @param sugarPrice - цена сахара
     * @param meatPrice - цена мяса
     * @param fishPrice - цена рыбы
     * @param milkPrice - цена молока
     * @param eggsPrice - цена яиц
     * @param butterPrice - цена масла
     * @param teaPrice - цена чая
     * @return - возвращает значения
     */

    double calc(double bakeryPrice, double pastaAndCerealsPrice, double potatoPrice,
                double vegetablesPrice, double fruitPrice, double sugarPrice, double meatPrice,
                double fishPrice, double milkPrice, double eggsPrice, double butterPrice,
                double teaPrice) {
        return bakeryPrice * Person.getGroup().getBakery() +
                pastaAndCerealsPrice * Person.getGroup().getPastaAndCereals() +
                potatoPrice * Person.getGroup().getPotato() +
                vegetablesPrice * Person.getGroup().getVegetables() +
                fruitPrice * Person.getGroup().getFruit() +
                sugarPrice * Person.getGroup().getSugar() +
                meatPrice * Person.getGroup().getMeat() +
                fishPrice * Person.getGroup().getFish() +
                milkPrice * Person.getGroup().getMilk() +
                eggsPrice * Person.getGroup().getEggs() +
                butterPrice * Person.getGroup().getButter() +
                teaPrice * Person.getGroup().getTea();
    }

    @FXML
    void showResult(ActionEvent event) {
        groupLabel.setText(Person.getGroup().getName());
        bakeryLabel.setText(String.valueOf(Person.getGroup().getBakery()));
        pastaAndCerealsLabel.setText(String.valueOf(Person.getGroup().getPastaAndCereals()));
        potatoLabel.setText(String.valueOf(Person.getGroup().getPotato()));
        vegetablesLable.setText(String.valueOf(Person.getGroup().getVegetables()));
        fruitLabel.setText(String.valueOf(Person.getGroup().getFruit()));
        sugarLabel.setText(String.valueOf(Person.getGroup().getSugar()));
        meatLabel.setText(String.valueOf(Person.getGroup().getMeat()));
        fishLabel.setText(String.valueOf(Person.getGroup().getFish()));
        milkLabel.setText(String.valueOf(Person.getGroup().getMilk()));
        eggsLabel.setText(String.valueOf(Person.getGroup().getEggs()));
        butterLabel.setText(String.valueOf(Person.getGroup().getButter()));
        teaLabel.setText(String.valueOf(Person.getGroup().getTea()));

        try {
            double bakeryPrice = Double.valueOf(bakeryInput.getText());
            double pastaAndCerealsPrice = Double.valueOf(pastaAndCerealsInput.getText());
            double potatoPrice = Double.valueOf(potatoInput.getText());
            double vegetablesPrice = Double.valueOf(vegetablesInput.getText());
            double fruitPrice = Double.valueOf(fruitInput.getText());
            double sugarPrice = Double.valueOf(sugarInput.getText());
            double meatPrice = Double.valueOf(meatInput.getText());
            double fishPrice = Double.valueOf(fishInput.getText());
            double milkPrice = Double.valueOf(milkInput.getText());
            double eggsPrice = Double.valueOf(eggsInput.getText());
            double butterPrice = Double.valueOf(butterInput.getText());
            double teaPrice = Double.valueOf(teaInput.getText());

            double result = calc(bakeryPrice, pastaAndCerealsPrice, potatoPrice, vegetablesPrice,
                    fruitPrice, sugarPrice, meatPrice, fishPrice, milkPrice, eggsPrice, butterPrice, teaPrice);
            resultLabel.setText(String.valueOf(result));

        } catch (Exception e) {
            ErrorWindow.openWindow("Невалидные данные");
        }
    }

    @FXML
    void writeDB(ActionEvent event) {
        try {
            double bakeryPrice = Double.valueOf(bakeryInput.getText());
            double pastaAndCerealsPrice = Double.valueOf(pastaAndCerealsInput.getText());
            double potatoPrice = Double.valueOf(potatoInput.getText());
            double vegetablesPrice = Double.valueOf(vegetablesInput.getText());
            double fruitPrice = Double.valueOf(fruitInput.getText());
            double sugarPrice = Double.valueOf(sugarInput.getText());
            double meatPrice = Double.valueOf(meatInput.getText());
            double fishPrice = Double.valueOf(fishInput.getText());
            double milkPrice = Double.valueOf(milkInput.getText());
            double eggsPrice = Double.valueOf(eggsInput.getText());
            double butterPrice = Double.valueOf(butterInput.getText());
            double teaPrice = Double.valueOf(teaInput.getText());
            new DataBase().insert(bakeryPrice, pastaAndCerealsPrice, potatoPrice,
                    vegetablesPrice, fruitPrice, sugarPrice, meatPrice, fishPrice,
                    milkPrice, eggsPrice, butterPrice, teaPrice, Person.getName());
        } catch (SQLException sqlException) {
            ErrorWindow.openWindow("Ошибка Базы Данных");
        } catch (Exception e) {
            ErrorWindow.openWindow("Невалидные данные");
        }
    }
}
