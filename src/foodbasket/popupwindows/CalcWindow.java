package foodbasket.popupwindows;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Класс отвечающий за вывод окна с заполнением
 */

public class CalcWindow {
    public void open() throws IOException {
        Stage primaryStage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/foodbasket/views/calcWindow.fxml"));
        primaryStage.setTitle("Заполнение");
        primaryStage.setScene(new Scene(root, 650, 500));
        primaryStage.show();
    }
}
