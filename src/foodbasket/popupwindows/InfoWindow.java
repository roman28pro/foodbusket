package foodbasket.popupwindows;

import foodbasket.entities.Group;
import foodbasket.utils.InfoTableCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Класс отвечающий за вывод окна с информацией
 */

public class InfoWindow {
    public static void open(){
        ObservableList<InfoTableCell> data = FXCollections.observableArrayList();
        data.add(new InfoTableCell("Хлебобулочные",
                Group.HIGH_BUDGET.getBakery(),
                Group.AVERAGE_BUDGET.getBakery(),
                Group.LOW_BUDGET.getBakery()));

        data.add(new InfoTableCell("Макароны и крупы",
                Group.HIGH_BUDGET.getPastaAndCereals(),
                Group.AVERAGE_BUDGET.getPastaAndCereals(),
                Group.LOW_BUDGET.getPastaAndCereals()));

        data.add(new InfoTableCell("Картофель",
                Group.HIGH_BUDGET.getPotato(),
                Group.AVERAGE_BUDGET.getPotato(),
                Group.LOW_BUDGET.getPotato()));

        data.add(new InfoTableCell("Овощи",
                Group.HIGH_BUDGET.getVegetables(),
                Group.AVERAGE_BUDGET.getVegetables(),
                Group.LOW_BUDGET.getVegetables()));

        data.add(new InfoTableCell("Фрукты",
                Group.HIGH_BUDGET.getFruit(),
                Group.AVERAGE_BUDGET.getFruit(),
                Group.LOW_BUDGET.getFruit()));

        data.add(new InfoTableCell("Сахар",
                Group.HIGH_BUDGET.getSugar(),
                Group.AVERAGE_BUDGET.getSugar(),
                Group.LOW_BUDGET.getSugar()));

        data.add(new InfoTableCell("Мясо",
                Group.HIGH_BUDGET.getMeat(),
                Group.AVERAGE_BUDGET.getMeat(),
                Group.LOW_BUDGET.getMeat()));

        data.add(new InfoTableCell("Рыба",
                Group.HIGH_BUDGET.getFish(),
                Group.AVERAGE_BUDGET.getFish(),
                Group.LOW_BUDGET.getFish()));

        data.add(new InfoTableCell("Молочные продукты",
                Group.HIGH_BUDGET.getMilk(),
                Group.AVERAGE_BUDGET.getMilk(),
                Group.LOW_BUDGET.getMilk()));

        data.add(new InfoTableCell("Яйца",
                Group.HIGH_BUDGET.getEggs(),
                Group.AVERAGE_BUDGET.getEggs(),
                Group.LOW_BUDGET.getEggs()));

        data.add(new InfoTableCell("Масла и жиры",
                Group.HIGH_BUDGET.getButter(),
                Group.AVERAGE_BUDGET.getButter(),
                Group.LOW_BUDGET.getButter()));

        data.add(new InfoTableCell("Чай",
                Group.HIGH_BUDGET.getTea(),
                Group.AVERAGE_BUDGET.getTea(),
                Group.LOW_BUDGET.getTea()));

        TableView<InfoTableCell> table = new TableView<InfoTableCell>(data);

        TableColumn<InfoTableCell, String> nameColumn
                = new TableColumn<InfoTableCell, String>("Название");

        TableColumn<InfoTableCell, Double> highColumn
                = new TableColumn<InfoTableCell, Double>("Высокий бюджет");

        TableColumn<InfoTableCell, Double> averageColumn
                = new TableColumn<InfoTableCell, Double>("Средний бюджет");

        TableColumn<InfoTableCell, Double> lowColumn
                = new TableColumn<InfoTableCell, Double>("Низкий бюджет");


        table.getColumns().addAll(nameColumn, highColumn, averageColumn, lowColumn);

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        highColumn.setCellValueFactory(new PropertyValueFactory<>("highBudgetValue"));
        averageColumn.setCellValueFactory(new PropertyValueFactory<>("averageBudgetValue"));
        lowColumn.setCellValueFactory(new PropertyValueFactory<>("lowBudget"));

        table.setItems(data);


        StackPane root = new StackPane();
        root.setPadding(new Insets(10));
        root.getChildren().add(table);

        Stage stage = new Stage();
        stage.setTitle("Меню");

        Scene scene = new Scene(root, 380, 300);
        stage.setScene(scene);
        stage.show();
    }
}
