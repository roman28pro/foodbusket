package foodbasket.popupwindows;

import foodbasket.entities.DBRecord;
import foodbasket.utils.DataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.sql.SQLException;

/**
 * Класс отвечающий за вывод окна с базой данных
 */

public class DBWindow {
    public static void openWindow() {
        ObservableList<DBRecord> data = null;
        try {
            data = FXCollections.observableArrayList(new DataBase().select());
        } catch (SQLException sqlException) {
            ErrorWindow.openWindow("Ошибка Дазы Данных");
            return;
        }

        TableColumn<DBRecord, String> name
                = new TableColumn<DBRecord, String>("Имя пользователя");

        TableView<DBRecord> table = new TableView<DBRecord>(data);

        TableColumn<DBRecord, Double> bakeryPrice
                = new TableColumn<DBRecord, Double>("Хлебобулочные цена");

        TableColumn<DBRecord, Double> pastaAndCerealsPrice
                = new TableColumn<DBRecord, Double>("Макароны и крупы цена");

        TableColumn<DBRecord, Double> potatoPrice
                = new TableColumn<DBRecord, Double>("Картофель цена");

        TableColumn<DBRecord, Double> vegetablesPrice
                = new TableColumn<DBRecord, Double>("Овощи цена");

        TableColumn<DBRecord, Double> fruitPrice
                = new TableColumn<DBRecord, Double>("Фрукты цена");

        TableColumn<DBRecord, Double> sugarPrice
                = new TableColumn<DBRecord, Double>("Сахар цена");

        TableColumn<DBRecord, Double> meatPrice
                = new TableColumn<DBRecord, Double>("Мясо цена");

        TableColumn<DBRecord, Double> fishPrice
                = new TableColumn<DBRecord, Double>("Рыба цена");

        TableColumn<DBRecord, Double> milkPrice
                = new TableColumn<DBRecord, Double>("Молоко цена");

        TableColumn<DBRecord, Double> eggsPrice
                = new TableColumn<DBRecord, Double>("Яйца цена");

        TableColumn<DBRecord, Double> butterPrice
                = new TableColumn<DBRecord, Double>("Масло цена");

        TableColumn<DBRecord, Double> teaPrice
                = new TableColumn<DBRecord, Double>("Чай цена");

        table.getColumns().addAll(name, bakeryPrice, pastaAndCerealsPrice, potatoPrice,
                vegetablesPrice, fruitPrice, sugarPrice, meatPrice, fishPrice,
                milkPrice, eggsPrice, butterPrice, teaPrice);

        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        bakeryPrice.setCellValueFactory(new PropertyValueFactory<>("bakeryPrice"));
        pastaAndCerealsPrice.setCellValueFactory(new PropertyValueFactory<>("pastaAndCerealsPrice"));
        potatoPrice.setCellValueFactory(new PropertyValueFactory<>("potatoPrice"));
        vegetablesPrice.setCellValueFactory(new PropertyValueFactory<>("vegetablesPrice"));
        fruitPrice.setCellValueFactory(new PropertyValueFactory<>("fruitPrice"));
        sugarPrice.setCellValueFactory(new PropertyValueFactory<>("sugarPrice"));
        meatPrice.setCellValueFactory(new PropertyValueFactory<>("meatPrice"));
        fishPrice.setCellValueFactory(new PropertyValueFactory<>("fishPrice"));
        milkPrice.setCellValueFactory(new PropertyValueFactory<>("milkPrice"));
        eggsPrice.setCellValueFactory(new PropertyValueFactory<>("eggsPrice"));
        butterPrice.setCellValueFactory(new PropertyValueFactory<>("butterPrice"));
        teaPrice.setCellValueFactory(new PropertyValueFactory<>("teaPrice"));

        table.setItems(data);


        StackPane root = new StackPane();
        root.setPadding(new Insets(10));
        root.getChildren().add(table);

        Stage stage = new Stage();
        stage.setTitle("База Данных");

        Scene scene = new Scene(root, 720, 300);
        stage.setScene(scene);
        stage.show();
    }
}
