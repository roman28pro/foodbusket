package foodbasket;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Kireev Roman 17IT18
 */

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/foodbasket/views/main.fxml"));
        primaryStage.setTitle("Главная");
        primaryStage.setScene(new Scene(root, 600, 300));
        primaryStage.show();
    }
}

