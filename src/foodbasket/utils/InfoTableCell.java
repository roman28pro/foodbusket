package foodbasket.utils;

public class InfoTableCell {
    private String name;
    private double highBudgetValue;
    private double averageBudgetValue;
    private double lowBudget;

    public InfoTableCell() {
    }

    public InfoTableCell(String name, double highBudgetValue, double averageBudgetValue, double lowBudget) {
        this.name = name;
        this.highBudgetValue = highBudgetValue;
        this.averageBudgetValue = averageBudgetValue;
        this.lowBudget = lowBudget;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHighBudgetValue() {
        return highBudgetValue;
    }

    public void setHighBudgetValue(double highBudgetValue) {
        this.highBudgetValue = highBudgetValue;
    }

    public double getAverageBudgetValue() {
        return averageBudgetValue;
    }

    public void setAverageBudgetValue(double averageBudgetValue) {
        this.averageBudgetValue = averageBudgetValue;
    }

    public double getLowBudget() {
        return lowBudget;
    }

    public void setLowBudget(double lowBudget) {
        this.lowBudget = lowBudget;
    }
}
