package foodbasket.utils;

import foodbasket.entities.Group;

/**
 * Класс отвечающий за соотношение пользователя к социальной группе
 */

public class GroupFactory {
    public static Group getGroup(int age) {
        if (age < 18) {
            return Group.AVERAGE_BUDGET;
        } else if(age < 65) {
            return Group.HIGH_BUDGET;
        } else {
            return Group.LOW_BUDGET;
        }
    }
}
