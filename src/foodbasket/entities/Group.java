package foodbasket.entities;

public enum Group {
    LOW_BUDGET("Низкий бюджет", 20, 30, 12, 15, 25, 30, 250, 150, 40, 35, 60, 500),
    AVERAGE_BUDGET("Средний бюджет", 25, 50, 15, 20, 30, 50, 350, 250, 60, 50, 90, 800),
    HIGH_BUDGET("Высокий бюджет", 30, 100, 20, 30, 35, 100, 500, 400, 100, 70, 150, 1500);

    private final String name;
    private final double bakery;
    private final double pastaAndCereals;
    private final double potato;
    private final double vegetables;
    private final double fruit;
    private final double sugar;
    private final double meat;
    private final double fish;
    private final double milk;
    private final double eggs;
    private final double butter;
    private final double tea;

    /**
     * Идет соотношение группы людей и цены на товары
     * @param name
     * @param bakery
     * @param pastaAndCereals
     * @param potato
     * @param vegetables
     * @param fruit
     * @param sugar
     * @param meat
     * @param fish
     * @param milk
     * @param eggs
     * @param butter
     * @param tea
     */

    Group(String name, double bakery, double pastaAndCereals, double potato,
          double vegetables, double fruit, double sugar,
          double meat, double fish, double milk, double eggs,
          double butter, double tea) {
        this.name = name;
        this.bakery = bakery;
        this.pastaAndCereals = pastaAndCereals;
        this.potato = potato;
        this.vegetables = vegetables;
        this.fruit = fruit;
        this.sugar = sugar;
        this.meat = meat;
        this.fish = fish;
        this.milk = milk;
        this.eggs = eggs;
        this.butter = butter;
        this.tea = tea;
    }

    public String getName() {
        return name;
    }

    public double getBakery() {
        return bakery;
    }

    public double getPastaAndCereals() {
        return pastaAndCereals;
    }

    public double getPotato() {
        return potato;
    }

    public double getVegetables() {
        return vegetables;
    }

    public double getFruit() {
        return fruit;
    }

    public double getSugar() {
        return sugar;
    }

    public double getMeat() {
        return meat;
    }

    public double getFish() {
        return fish;
    }

    public double getMilk() {
        return milk;
    }

    public double getEggs() {
        return eggs;
    }

    public double getButter() {
        return butter;
    }

    public double getTea() {
        return tea;
    }
}
